'use strict';

var gulp = require('gulp');
var debug = require('gulp-debug');

gulp.task('default', function () {
    return gulp.src('foo.js')
        .pipe(debug({title: 'unicorn:'}))
        .pipe(gulp.dest('dist'));
});
