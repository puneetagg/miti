// export class AuthInterceptorService {
//   constructor($log, localStorageService) {
//     'ngInject';
//     // this.$log = $log;
//     this.localStorageService = localStorageService;
//   }
//
//   request = (config) => {
//     config.headers = config.headers || {};
//     if (this.localStorageService.get('profile')) {
//       config.headers.Authorisation = 'Bearer ' + this.localStorageService.get('profile');
//     }
//     return config;
//   }
//
//   response = (response) => {
//     if (response.status === 401) {
//       // this.$log.log(response);
//     }
//     return response;
//   }
// }

class HttpInterceptor {
  constructor() {
    ['request', 'requestError', 'response', 'responseError']
    .forEach((method) => {
      if (this[method]) {
        this[method] = this[method].bind(this);
      }
    });
  }
}

export class AuthInterceptorService extends HttpInterceptor {

  constructor($q, localStorageService) {
    'ngInject';
    super();
    this.$q = $q;
    this.localStorageService = localStorageService;
  }

  request(config) {
      let token;
      config.headers = config.headers || {};
      token = this.localStorageService.get('prof')
      if (token) {
        config.headers.Authorization = 'JWT ' + token;
      }
      return config;
    }
    //
    // response(res){
    //
    // }

  responseError(rejection) {
    var authToken = rejection.config.headers.Authorization;
    if (rejection.status === 401 && !authToken) {
      // let authentication_url = rejection.data.errors[0].data.authenticationUrl;
      // this.$window.location.replace(authentication_url);
      return this.$q.defer(rejection);
    }
    return this.$q.reject(rejection);
  }
}
