export class AuthenticationService {
  constructor($http, $log, $cookies, $window, $state, $location, SessionService) {
    'ngInject';
    this.$http = $http;
    this.$log = $log;
    this.$cookies = $cookies;
    this.$window = $window;
    this.$state = $state;
    this.$location = $location;
    this.sessionService = SessionService;
  }


  login(user_data) {
    return this.$http({
        method: 'POST',
        url: 'http://localhost:8000/login',
        data: user_data,
        headers: {
          "Content-Type": "application/json",
          'X-CSRFToken': this.$cookies.get('csrftoken')
        }
      })
      .then((responseData) => {
        let data = responseData.data
        this.$log.log(data);
        this.sessionService.setAuthData(data.prof);
        this.$location.path('/');
        // this.$window.location.href;
        this.$window.location = '/';
        // this.$window.location.reload();
        this.$state.go('home', {}, {
          reload: true
        });
      });
  }

  logoutUser() {
    this.sessionService.destroy();
  }

  isLoggedIn() {
    return this.sessionService.getAuthToken() !== null;
  }

}
