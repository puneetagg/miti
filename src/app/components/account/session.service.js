export class SessionService {
  constructor($http, $log, $cookies, $window, $state, $location, localStorageService) {
    'ngInject';
    this.$http = $http;
    this.$log = $log;
    this.$cookies = $cookies;
    this.$window = $window;
    this.$state = $state;
    this.$location = $location;
    this.localStorageService = localStorageService;
    this.loggedIn = 0;
    this.authToken = localStorageService.get('prof');
    this.userData = angular.fromJson(localStorageService.get('usrDt'));
  }

  setAuthData(data) {
    this.setAuthToken(data);
    this.setUserData();
  }

  setAuthToken(data) {
    this.localStorageService.set('prof', data);
    this.authToken = data;
    return;
  }

  setUserData() {
    if (this.authToken != null) {
      let encodedUserData = this.authToken.split('.')[1];
      encodedUserData = encodedUserData.replace('-', '+').replace('_', '/');
      let userData = this.$window.atob(encodedUserData);
      this.localStorageService.set('usrDt', userData);
      this.userData = angular.fromJson(userData);
    }
    return;
  }

  getAuthToken() {
    return this.authToken;
  }

  getFullName() {
    return this.userData != null ? this.userData['full_name'] : null;
  }

  getUsername() {
    return this.userData != null ? this.userData['username'] : null;
  }

  getIdentifiers() {
    return this.userData != null ? this.userData['contact'] : null;
  }

  destroy() {
    this.localStorageService.remove('prof');
    this.localStorageService.remove('usrDt');
    return;
  }

}
