export function IdentifierDirective() {
  'ngInject';

  let directive = {
    restrict: 'E',
    templateUrl: 'app/components/identifier/identifier.html',
    scope: true,
    controller: IdentifierController,
    controllerAs: 'identifier',
    bindToController: true
  };

  return directive;
}

class IdentifierController {
  constructor(IdentifierService) {
    'ngInject';

    this.identifierList = IdentifierService.getIdentifiers()
  }

}
