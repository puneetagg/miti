export class IdentifierService {
  constructor(SessionService) {
    'ngInject';
    this.sessionService = SessionService
  }

  getIdentifiers() {
    return this.sessionService.getIdentifiers()
  }

}
