export function LoginDirective() {
  'ngInject';

  let directive = {
    restrict: 'E',
    templateUrl: 'app/components/login/login.html',
    // scope: {
    //     post: '='
    // },
    scope: true,
    controller: LoginController,
    controllerAs: 'login',
    bindToController: true
  };
  return directive;
}

class LoginController {
  constructor($http, $log, $cookies, $window, $auth, AuthenticationService, localStorageService) {
    'ngInject';
    this.$http = $http;
    this.$log = $log;
    this.$cookies = $cookies;
    this.$window = $window;
    this.$auth = $auth;
    this.authenticationService = AuthenticationService
    this.localStorageService = localStorageService;
  }

  loginUser() {
    return this.authenticationService.login(this.user_data)
  }

  authenticate(provider){
    this.$auth.authenticate(provider);
  }

}
