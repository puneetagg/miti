export function LogoutController(AuthenticationService) {
    'ngInject';
    return AuthenticationService.logoutUser()
}
