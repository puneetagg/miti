export function NavbarDirective() {
  'ngInject';

  let directive = {
    restrict: 'E',
    templateUrl: 'app/components/navbar/navbar.html',
    scope: {
      creationDate: '='
    },
    controller: NavbarController,
    controllerAs: 'vm',
    bindToController: true
  };

  return directive;
}

class NavbarController {
  constructor(AuthenticationService, SessionService) {
    'ngInject';

    this.authenticationService = AuthenticationService;
    this.sessionService = SessionService;

    this.loggedIn = this.authenticationService.isLoggedIn();
    if (this.loggedIn) {
      this.full_name = this.sessionService.getFullName();
    }
  }

  logout() {
    this.authenticationService.logoutUser();
  }
}
