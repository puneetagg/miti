export function PostMessageDirective() {
  'ngInject';

  let directive = {
    restrict: 'E',
    templateUrl: 'app/components/postMessage/postMessage.html',
    scope: true,
    controller: PostMessageController,
    controllerAs: 'postMessage',
    bindToController: true
  };
  return directive;
}

class PostMessageController {
  constructor($http, $log, $cookies) {
    'ngInject';
    this.$http = $http;
    this.$log = $log;
    this.$cookies = $cookies;
  }

  submitPost() {
    this.$http({
        method: 'POST',
        url: 'http://localhost:8000/vaio/post',
        data: this.post,
        headers: {
          "Content-Type": "application/json",
          'X-CSRFToken': this.$cookies.get('csrftoken')
        }
      })
      .then((data) => {
        this.$log.log(data);
      });
  }
}
