export function PostTupleDirective() {
  'ngInject';

  let directive = {
    restrict: 'E',
    templateUrl: 'app/components/postWall/postTuple.html',
    // scope: {
    //     post: '='
    // },
    scope: true,
    controller: PostTupleController,
    controllerAs: 'vm',
    bindToController: true
  };

  return directive;
}

class PostTupleController {
  constructor() {
    'ngInject';
    // "this.creation" is available by directive option "bindToController: true"
    // this.relativeDate = moment(this.creationDate).fromNow();
    // this.ref_user.org_name = this.post.ref_user.full_name;
    // this.message = this.post.message;
  }
}
