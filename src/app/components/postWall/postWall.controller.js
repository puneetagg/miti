export class PostWallController {
  constructor($http, PostWallService) {
    'ngInject';
    this.$http = $http;

    PostWallService.getPosts()
      .then((response)=>{
        this.postList = response.data;
    }
    );
  }
}
