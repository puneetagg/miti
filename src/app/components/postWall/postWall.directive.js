export function PostWallDirective() {
  'ngInject';

  let directive = {
    restrict: 'E',
    templateUrl: 'app/components/postWall/postWall.html',
    // scope: {
    //     post: '='
    // },
    scope: true,
    controller: PostWallController,
    controllerAs: 'postWall',
    bindToController: true
  };
  return directive;
}

class PostWallController {
  constructor($http, PostWallService) {
    'ngInject';
    this.$http = $http;

    PostWallService.getPosts()
      .then((response) => {
        this.postList = response.data;
      });
  }
}
