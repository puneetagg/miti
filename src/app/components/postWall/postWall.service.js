export class PostWallService {
  constructor ($http, $cookies) {
    'ngInject';
    this.$http = $http;
    this.$cookies = $cookies;
  }

  getPosts(){
    return this.$http({
        method: 'GET',
        url: 'http://localhost:8000/vaio/post',
        headers: {
          "Content-Type": "application/json",
          'X-CSRFToken': this.$cookies.get('csrftoken')
        }
      });
  }

}
