export class SignupController {
  constructor($http, $log, $cookies) {
    'ngInject';
    this.$http = $http;
    this.$log = $log;
    this.$cookies = $cookies;
  }

  submitForm() {
    this.$http({
        method: 'POST',
        // url: 'http://localhost:8000/signup',
        url: '/signup/create',
        data: this.post,
        // data: $.param(this.post),
        headers: {
          "Content-Type": "application/json",
          'X-CSRFToken': this.$cookies.get('csrftoken')
        }
      })
      .then((data) => {
        this.$log.log(data);
      });
  }
}
