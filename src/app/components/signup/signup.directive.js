export function SignupDirective() {
  'ngInject';

  let directive = {
    restrict: 'E',
    templateUrl: 'app/components/signup/signup.html',
    // scope: {
    //     post: '='
    // },
    scope: true,
    controller: SignupController,
    controllerAs: 'signup',
    bindToController: true
  };
  return directive;
}

class SignupController {
  constructor($http, $log, $cookies) {
    'ngInject';
    this.$http = $http;
    this.$log = $log;
    this.$cookies = $cookies;
  }

  createUser() {
    this.$http({
        method: 'POST',
        url: 'http://localhost:8000/signup',
        data: this.user_data,
        headers: {
          "Content-Type": "application/json",
          'X-CSRFToken': this.$cookies.get('csrftoken')
        }
      })
      .then((data) => {
        this.$log.log(data);
      });
  }
}
