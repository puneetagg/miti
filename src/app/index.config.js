export function config($logProvider, $httpProvider, $locationProvider, $authProvider, toastrConfig, localStorageServiceProvider) {
  'ngInject';
  // Enable log
  $logProvider.debugEnabled(true);
  $locationProvider.html5Mode({
    enabled: true,
    requireBase: false
  });
  // $httpProvider.defaults.useXDomain = true;
  delete $httpProvider.defaults.headers.common['X-Requested-With'];
  $httpProvider.defaults.xsrfCookieName = 'csrftoken';
  $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
  $httpProvider.interceptors.push('AuthInterceptorService');
  // $httpProvider.defaults.withCredentials = true,
  // Set options third-party lib
  toastrConfig.allowHtml = true;
  toastrConfig.timeOut = 3000;
  toastrConfig.positionClass = 'toast-top-right';
  toastrConfig.preventDuplicates = true;
  toastrConfig.progressBar = true;
  localStorageServiceProvider.setPrefix('miti');

  $authProvider.facebook({
    clientId: '785143024925316'
  });
}
