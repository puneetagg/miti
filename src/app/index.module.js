import { config } from './index.config';
import { routerConfig } from './index.route';
import { runBlock } from './index.run';
// import { PostMessageController } from '../app/components/postMessage/postMessage.controller';
import { MainController } from '../app/main/main.controller';
import { SignupDirective } from '../app/components/signup/signup.directive';
import { LoginDirective } from '../app/components/login/login.directive';
import { LogoutController } from '../app/components/logout/logout.controller';
import { PostWallService } from '../app/components/postWall/postWall.service';
import { AuthInterceptorService } from '../app/components/account/authInterceptor.service';
import { AuthenticationService } from '../app/components/account/authentication.service';
import { SessionService } from '../app/components/account/session.service';
import { NavbarDirective } from '../app/components/navbar/navbar.directive';
import { PostWallDirective } from '../app/components/postWall/postWall.directive';
import { PostMessageDirective } from '../app/components/postMessage/postMessage.directive';
import { IdentifierDirective } from '../app/components/identifier/identifier.directive';
import { IdentifierService } from '../app/components/identifier/identifier.service';
import { PostTupleDirective } from '../app/components/postWall/postTuple.directive';

angular.module('miti', ['ngAnimate', 'ngTouch', 'ngMessages','ngCookies', 'ngResource', 'ui.router', 'ui.bootstrap', 'satellizer', 'toastr', 'LocalStorageModule'])
  .config(config)
  .config(routerConfig)
  .run(runBlock)
  .controller('MainController', MainController)
  .controller('LogoutController', LogoutController)
  .service('PostWallService', PostWallService)
  .service('AuthInterceptorService', AuthInterceptorService)
  .service('AuthenticationService', AuthenticationService)
  .service('SessionService', SessionService)
  .directive('signupForm', SignupDirective)
  .directive('loginForm', LoginDirective)
  .directive('navBar', NavbarDirective)
  .directive('postWall', PostWallDirective)
  .directive('postMessage', PostMessageDirective)
  .directive('postTuple', PostTupleDirective)
  .directive('sharedIdentifier', IdentifierDirective)
  .service('IdentifierService', IdentifierService);
