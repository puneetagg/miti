export function routerConfig($stateProvider, $urlRouterProvider) {
  'ngInject';
  $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'app/main/main.html',
      controller: 'MainController',
      controllerAs: 'main'
    }).state('signup', {
      url: '/signup',
      templateUrl: 'app/components/signup/signupPage.html'
      // controller: 'SignupController',
      // controllerAs: 'signup'
    }).state('login', {
      url: '/login',
      templateUrl: 'app/components/login/loginPage.html'
      // controller: 'LoginController',
      // controllerAs: 'login'
    }).state('logout', {
      url: '/logout',
      controller: 'LogoutController',
      controllerAs: 'logout'
    //   templateUrl: 'app/main/main.html',
    });

  $urlRouterProvider.otherwise('/');
}
